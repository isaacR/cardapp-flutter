import 'package:flutter/material.dart';

class Login extends StatefulWidget {
  Login({Key key}) : super(key: key);

  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  static final color = Color.fromRGBO(14, 57, 93, 1);
  static final colorTextField = Color.fromRGBO(0, 178, 178, 1);
  FocusNode _focusEmail = new FocusNode();
  FocusNode _focusPassword = new FocusNode();
  bool _changeColorEmail = false;
  bool _changeColorPassword = false;

  String _password = '';
  bool _obscureText = true;



  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _focusEmail.addListener(_onFocusChangeEmail);
    _focusPassword.addListener(_onFocusChangePassword);
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: color,
      body: ListView(
        children: <Widget>[
          _createLogo(),
                    _createEmail(),
                    _createPassword(),
                    _forgotPassword()
                  ],
                ),
              );
            }
          
            Widget _createEmail() {
              return TextField(
                focusNode: _focusEmail,
                style: TextStyle(color: Colors.white),
                keyboardType: TextInputType.emailAddress,
                decoration: InputDecoration(
                  labelText: "Email",
                  labelStyle: TextStyle(color: _changeColorEmail? colorTextField: Colors.white,
                  ),
                focusedBorder: UnderlineInputBorder(borderSide: BorderSide(color: colorTextField)),
                enabledBorder: UnderlineInputBorder(borderSide: BorderSide(color: Colors.grey)),
                ),
                onSubmitted: (value){
                  // input value
                },
              );
            }
          
            Widget _createPassword() {
              return Stack(
                children: [
                  TextFormField(
                    focusNode: _focusPassword,
                    style: TextStyle(color: Colors.white),
                    keyboardType: TextInputType.text,
                    decoration: InputDecoration(
                      labelText: "Password",
                      labelStyle: TextStyle(color: _changeColorPassword? colorTextField: Colors.white,
                  ),
                      focusedBorder: UnderlineInputBorder(borderSide: BorderSide(color: colorTextField)),
                enabledBorder: UnderlineInputBorder(borderSide: BorderSide(color: Colors.grey)),
                    ),
                    validator: (password) {
                      Pattern pattern =
                          r'^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[!@#\$&*~]).{8,}$';
                      RegExp regex = new RegExp(pattern);
                      if (password.isEmpty) {
                        return 'Please Enter Password';
                      } else if (!regex.hasMatch(password))
                        return 'Enter valid password';
                      else
                        return null;
                    },
                    //onSaved: (password) => _password = password,
                    onSaved: (value){
                      print('saved');
                    },
                    onChanged: (value2){
                      print(value2);
                    },
                    textInputAction: TextInputAction.done,
                    obscureText: _obscureText,
                    
                  ),
                  Positioned(
                    top: 2,
                    right: 10,
                    child: IconButton(
                        color: Colors.white,
                        icon: Icon(
                          _obscureText ? Icons.visibility_off : Icons.visibility,
                        ),
                        onPressed: () {
                          setState(() {
                            _obscureText = !_obscureText;
                          });
                        }),
                  ),
                ],
              );
            }
          
            Widget _forgotPassword() {
              return TextButton(
                  style: ButtonStyle(alignment: Alignment.centerRight),
                  child: Text('Olvide mi contraseña'),
                  onPressed: () {});
            }
          
          
            void _onFocusChangeEmail(){
              setState(() {
                _changeColorEmail = !_changeColorEmail;
              });
            }
          
             void _onFocusChangePassword(){
              setState(() {
                _changeColorPassword = !_changeColorPassword;
              });
            }
          
           Widget _createLogo() {
             return Image.asset('assets/logo1.jpg');
            }
}
