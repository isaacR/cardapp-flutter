import 'package:cardapp/Views/slider.dart';
import 'package:flutter/material.dart';



class Tutorial extends StatefulWidget {
  Tutorial({Key key}) : super(key: key);
  @override
  _TutorialState createState() => _TutorialState();
}

class _TutorialState extends State<Tutorial> {
   int _currentPage = 0;
  static final color = Color.fromRGBO(14, 57, 93, 1);
  PageController _controller = PageController();

  List<Widget> _pages = [
    SliderPage(
        description:
            "Recibe pagos de tus contactos en otros bancos y usa tus tarjetas en cualquier establecimiento",
        image: "assets/image1.jpg"),
    SliderPage(
        description:
            "Ten siempre a la mano tus movimientos de cada una de tus tarjetas PrimeTime",
        image: "assets/image2.jpg"),
  ];
  @override
  Widget build(BuildContext context) {
   return Scaffold(
      body: Stack(
        children: <Widget>[
          PageView.builder(
            scrollDirection: Axis.horizontal,
            onPageChanged: _onchanged,
            controller: _controller,
            itemCount: _pages.length,
            itemBuilder: (context, int index) {
              return _pages[index];
            },
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: List<Widget>.generate(_pages.length, (int index) {
                    return AnimatedContainer(
                        duration: Duration(milliseconds: 300),
                        height: 10,
                        width: 10,
                        margin:
                            EdgeInsets.symmetric(horizontal: 5, vertical: 30),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(5),
                            color: (index == _currentPage)
                                ? color
                                : color.withOpacity(0.5)));
                  })),
              InkWell(
                onTap: () {
                  if (_controller.page == 0) {
                    _controller.nextPage(
                        duration: Duration(milliseconds: 800),
                        curve: Curves.easeInOutQuint);
                  } else {
                    print('Close tutorial view');
                  }
                },
                child: AnimatedContainer(
                    alignment: Alignment.center,
                    duration: Duration(milliseconds: 300),
                    height: 50,
                    width: (_currentPage == (_pages.length - 1)) ? 300 : 300,
                    decoration: BoxDecoration(
                        color: color, //Colors.blue,
                        borderRadius: BorderRadius.circular(30)),
                    child: (_currentPage == (_pages.length - 1))
                        ? Text(
                            "Registrate",
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 20,
                            ),
                          )
                        : Text(
                            "Siguiente",
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 20,
                            ),
                          )),
              ),
              SizedBox(
                height: 50,
              )
            ],
          ),
        ],
      ),
    );
  }

   _onchanged(int index) {
    setState(() {
      _currentPage = index;
    });
  }
}