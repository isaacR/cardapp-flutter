import 'package:flutter/material.dart';
//import 'package:cardapp/Views/tutorial.dart';
import 'package:cardapp/Views/login.dart';


void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Login(),
    );
  }
}